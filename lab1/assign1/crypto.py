#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Course: 531
Name: Both Akos
Nr: 1900
"""
import utils

shift_letters_with_size = lambda letter, n: chr((ord(letter) + n - ord('A')) % (ord('Z') - ord('A') + 1) + ord('A')) if letter.isupper() else letter
encode_letter_with_letter = lambda letter, code: chr(    ( ord(letter) - 2 * ord('A') + ord(code) )  %  (ord('Z') - ord('A') + 1) + ord('A')   )
decode_letter_with_letter = lambda letter, code: chr(    ( ord(letter) - ord(code) )  %  (ord('Z') - ord('A') + 1) + ord('A')   )

# Caesar Cipher

def encrypt_caesar(plaintext):
    encoded_text = ''
    for current_character in plaintext:
        encoded_text = encoded_text + shift_letters_with_size(current_character, 3)
    return encoded_text


def decrypt_caesar(ciphertext):
    decoded_text = ''
    for current_character in ciphertext:
        decoded_text = decoded_text + shift_letters_with_size(current_character, -3)
    return decoded_text


# Vigenere Cipher

def encrypt_vigenere(plaintext, keyword):
    encoded_text = ''
    for i in range(0, len(plaintext)):
        encoded_text = encoded_text + encode_letter_with_letter(plaintext[i], keyword[i%len(keyword)])
    return encoded_text


def decrypt_vigenere(ciphertext, keyword):
    decoded_text = ''
    for i in range(0, len(ciphertext)):
        decoded_text = decoded_text + decode_letter_with_letter(ciphertext[i], keyword[i%len(keyword)])
    return decoded_text


# Scytale Cipher

def encrypt_scytale(plaintext, circumference):
    encoded_text = ''
    processed_text = plaintext
    N = ((len(processed_text)-1)//circumference + 1)
    for i in range(len(plaintext), N*circumference):
        processed_text = processed_text + '0'

    for i in range(0, N*circumference):
        encoded_text = encoded_text + processed_text[(i%N)*circumference + i//N]

    return encoded_text.replace('0', '')


def decrypt_scytale(ciphertext, circumference):
    N = ((len(ciphertext) - 1) // circumference + 1)
    text_matrix = ['' for i in range(0, N)]

    dif = N*circumference - len(ciphertext)
    column = 0
    row = 0

    print(N)

    for i in range(0, len(ciphertext)):
        print(ciphertext[i])
        text_matrix[column] += ciphertext[i]
        column += 1
        if column == N or (column == N - 1 and row >= circumference - dif):
            column = 0
            row += 1

    return ''.join(text_matrix)


def encrypt_railfence(plaintext, num_rails):
    processed_text = plaintext
    size = (((len(plaintext)-2) // (2 * (num_rails - 1))) + 1) * (2 * (num_rails - 1)) + 1

    encoded_text = ''

    text_matrix = ['' for i in range(num_rails)]

    row_index = 0
    going_up = True

    for i in processed_text:
        text_matrix[row_index] += i
        if row_index == 0:
            going_up = False
        if row_index == num_rails - 1:
            going_up = True
        if going_up:
            row_index -= 1
        else:
            row_index += 1

    for text in text_matrix:
        encoded_text += text

    return encoded_text.replace('0', '')


def decrypt_railfence(ciphertext, num_rails):
    processed_text = ciphertext
    size = (((len(ciphertext) - 2) // (2 * (num_rails - 1))) + 1) * (2 * (num_rails - 1)) + 1
    number_of_missing_characters = size - len(ciphertext)

    decoded_text = ''

    row_size = (size-1)//(num_rails*2-2)

    text_matrix = ['' for i in range(num_rails)]
    text_matrix_index = [0 for i in range(num_rails)]

    index = row_size + 1

    if size != len(ciphertext):
        index -= 1

    text_matrix[0] = processed_text[0: index]

    if size != len(ciphertext):
        text_matrix[0] += '0'

    for i in range(1, len(text_matrix)-1):
        new_index = index+row_size*2
        if size - len(ciphertext) > i:
            new_index -= 1
        if len(ciphertext) - (size - 2 * (num_rails-1)) < i:
            new_index -= 1
        text_matrix[i] = processed_text[index:new_index]
        if size - len(ciphertext) > i:
            text_matrix[i] += '0'
        if len(ciphertext) - (size - 2 * (num_rails - 1)) < i:
            text_matrix[i] += '0'
        index = new_index

    new_index = index + row_size * 2

    if size - len(ciphertext) >= num_rails:
        new_index -= 1

    text_matrix[len(text_matrix)-1] = processed_text[index:new_index]

    if size - len(ciphertext) >= num_rails:
        text_matrix[len(text_matrix)-1] += '0'

    print(text_matrix)

    row_index = 0
    going_up = True

    for i in range(0, size):
        decoded_text = decoded_text + text_matrix[row_index][text_matrix_index[row_index]]
        text_matrix_index[row_index] += 1
        if row_index == 0:
            going_up = False
        if row_index == num_rails - 1:
            going_up = True
        if going_up:
            row_index -= 1
        else:
            row_index += 1

    return decoded_text.replace('0', '')